<head>
    <title>CeleProxy</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body style="margin: 0px">
    <?php
        $url = "www.yahoo.com";
        if (isset($_GET['url'])) $url = $_GET['url'];
    ?>
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0px">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">CeleProxy</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input name="url" type="text" class="form-control" placeholder="URL" width="100%" value="<?=$url?>">
                </div>
                <button type="submit" class="btn btn-default">Go</button>
            </form>
        </div>
    </nav>
    <iframe frameborder=0 border=0 cellspacing=0 width=100% height=100% src="mobile.php?<?=$url?>"></iframe>
</body>