<?php
include('libraries/log4php/2.3.0/Logger.php');
include('utils.php');

// mobile user-agent string
define('CELE_USERAGENT', 'Mozilla/5.0(iPad;iPhone;Android)');

// create logger
date_default_timezone_set('America/New_York');
Logger::configure('logs/config.xml');
$log = Logger::getLogger('Mobile Proxy');

if (isset($_GET["url"]) && isset($_GET["protocol"])) {

    // get target url
    $url = $_GET["url"];
    $protocol = $_GET["protocol"];
    $log->info($protocol);
    $log->info($url);

    // check mobile
    if (is_mobile() && string_endwith($url, '.mp4')) {
        header("Location: $protocol://$url");
    }

    // create request
    $request = array();
    foreach (apache_request_headers() as $name => $value) {
        if ($name != 'Host' && $name != 'User-Agent' && $name != 'Referer')
            array_push($request, "$name: $value");
    }

    // get header
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_USERAGENT, CELE_USERAGENT);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request);
    $header = curl_exec($curl);
    curl_close($curl);

    // set header
    foreach (preg_split("/((\r?\n)|(\r\n?))/", $header) as $line) header($line);
    header('X-Frame-Options:'); // allow iframe

    // curl content
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
    curl_setopt($curl, CURLOPT_USERAGENT, CELE_USERAGENT);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request);
    curl_exec($curl);
    curl_close($curl);

}
else {
    echo "<h1>Something is wrong!</h1>";
}